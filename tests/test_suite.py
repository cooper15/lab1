from src.login_module import Login
from src.form_functionality import Form


class TestExamples:
    def test_001_name_has_more_than_30_chars_message(self):
        form = Form()
        actual_message = form.enter_name("gfasdgasdgasdgasdfgasdgasdgasdgasdgasdgasdgasdgasdgasdgasdggsdfghsdfasdfhfghdfghdfghdfghdfghdfghdfghdfgh")
        assert actual_message == 'name has more than 30 chars'
